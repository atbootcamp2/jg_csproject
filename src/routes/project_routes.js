/*
 @project_routes.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/

'use strict'

const express = require('express');
const api = express.Router();
const ProjectController = require('./../controllers/project_controller');
const project_controller = new ProjectController();

// Project methods
api.get('/project', project_controller.get_projects);
api.get('/project/:id', project_controller.get_project_by_id);
api.post('/project', project_controller.save_project);
api.delete('/project/:id', project_controller.delete_project);

// Exports routes
module.exports = api;
