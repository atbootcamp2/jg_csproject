/*
 @utils_project_controller.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/


const EmptyOrNullValidation = require("./../validation_strategy/empty_or_null_validation");
const FolderOrFileDoesntExistValidation = require("./../validation_strategy/path_doesnt_exist_validation");
const LanguageExistsValidation = require("./../validation_strategy/language_exists_validation");
const ContextValidation = require("./../validation_strategy/context_validation");


class ValidationProjectContoller {
    
    // Validates data get_project_by_id
    validate_save_project(project_path, project_name, language) {
        const facade_strategies = [
            new EmptyOrNullValidation(project_name, "Project name"),
            new EmptyOrNullValidation(language, "Language"),
            new LanguageExistsValidation(language),
            new FolderOrFileDoesntExistValidation(project_path, 'Project')
        ];
        const context = new ContextValidation(facade_strategies);
        context.validate();
    }
}

// Exports class
module.exports = ValidationProjectContoller;
