/*
@ide_jala.js Copyright (c) 2021 Jalasoft
2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
All rights reserved
This software is the confidential and proprietary information of
Jalasoft , Confidential Information "). You shall not
disclose such Confidential Information and shall use it only in
accordance with the terms of the license agreement you entered into
with Jalasoft
*/


const ValidationCore = require('../../../common/validation_strategy/validation_core');


class Parameters{
    
    // Builds the Parameters class
    constructor(path_binary, path_projects, name_project){
        const validate = new ValidationCore();
        validate.parameters(path_binary, path_projects, name_project);
        this._path_binary = path_binary
        this._path_projects = path_projects
        this._name_project = name_project
    }

    // Returns the binary path
    get_path_binary(){
        return this._path_binary
    }

    // Returns the project path
    get_path_projects(){
        return this._path_projects
    }

    // Returns the project name
    get_name_project(){
        return this._name_project
    }
}

module.exports = Parameters